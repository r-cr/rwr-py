#!/bin/sh

INSTALL=${INSTALL:-"cp -pR"}
MKDIR=${MKDIR:-"mkdir -p"}
DESTDIR=${DESTDIR:-"${HOME}/rrr"}
PREFIX=${PREFIX:-} 
P=${P:-"rwr"}
DOCDIR=${DOCDIR:-"share/doc/${P}"}
EXAMPLEDIR=${EXAMPLEDIR:-"share/rwr"}

if [ -f conf/config.sh ] ; then
    . conf/config.sh
fi

install_general()
{
    args="$@"
    set -o xtrace
    ${MKDIR} "${DESTDIR}${PREFIX}/${owndir}"
    ${INSTALL} $args "${DESTDIR}${PREFIX}/${owndir}"
    set +o xtrace

}

install_bin()
{
    owndir='bin'
    install_general $owndir'/rwr'
    unset owndir
}
install_doc()
{
    owndir="${DOCDIR}"
    install_general \
        'README-rwr' \
        'doc/rwr-using.html' \
        'doc/rwr-using.txt'
    unset owndir
}

install_example()
{
    owndir="${EXAMPLEDIR}"
    install_general 'share/filter-example'
    unset owndir
}

set -o errexit

install_bin
install_doc
install_example
